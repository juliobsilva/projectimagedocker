resource "aws_instance" "creating_ec2" {
  ami = var.ec2_image_id
  instance_type = var.ec2_instance_type
  key_name = var.ec2_key_name 
  tags = {
    Name = var.ec2_tags
  } 
}

output "Ip_public" {
  value = aws_instance.creating_ec2.public_ip
}

output "Ip_private" {
  value = aws_instance.creating_ec2.private_ip
}