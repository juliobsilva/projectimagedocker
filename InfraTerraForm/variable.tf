variable "ec2_region" {
  default = "us-east-2"
}

variable "ec2_key_name" {
  default = "key_terraform"
}

variable "ec2_instance_type" {
  default = "t2.micro"
}

variable "ec2_image_id" {
  default = "ami-024e6efaf93d85776"
}

variable "ec2_tags" {
  default = "Infra Gitlabit"
}
